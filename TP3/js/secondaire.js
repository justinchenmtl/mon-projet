
function validerDate(debut, fin){
    var debut;
    var fin;
    if(debut > fin){
        return false;
    }else{
        return true;
    }
}

function validerSecondaire() {
    var derniereAnnee = document.getElementById("derniereAnnee").value;
    var derAnneeDebut = document.getElementById("derAnneeDebut").value;
    var derAnneeFin = document.getElementById("derAnneeFin").value;
    var nomDiplome = document.getElementById("nomDiplome").value;
    var diplomeExterieur = document.formSecondaire.diplomeExterieur.value;
    var secondaireDebut = document.getElementById("secondaireDebut").value;
    var secondaireFin = document.getElementById("secondaireFin").value;
    var mois = document.getElementById("mois").value;
    var annee = document.getElementById("annee").value;
    var discipline = document.getElementById("discipline").value;
    var institution = document.getElementById("institution").value;
    var pays = document.getElementById("pays").value;

    if(derniereAnnee != "" || derAnneeDebut != "" || derAnneeFin != ""){
        if(derAnneeDebut == ""){
            alert("Le début d'année est obligatoire!");
            document.formSecondaire.derAnneeDebut.focus();
            return false;
        }else if(derAnneeFin == ""){
            alert("La fin d'année est obligatoire!");
            document.formSecondaire.derAnneeFin.focus();
            return false;
        }
    }

    if(!validerDate(derAnneeDebut, derAnneeFin)){
        alert("La date de fin doit être supérieure à la date de début");
        document.formSecondaire.derAnneeDebut.focus();
        return false;
    }

    if(diplomeExterieur != "" || nomDiplome != "" || secondaireDebut != "" || secondaireFin != "" || mois != "" || annee != ""
       || discipline != "" || institution != "" || pays != ""){
        if(diplomeExterieur == ""){
            alert("Diplôme à l’extérieur du Québec est obligatoire");
            return false;
        }else if(nomDiplome == ""){
            alert("Le nom du diplôme est obligatoire");
            document.formSecondaire.nomDiplome.focus();
            return false;
        }else if(secondaireDebut == ""){
            alert("Le début d'année est obligatoire!");
            document.formSecondaire.secondaireDebut.focus();
            return false;
        }else if(secondaireFin == ""){
            alert("La fin d'année est obligatoire!");
            document.formSecondaire.secondaireFin.focus();
            return false;
        }else if(mois == ""){
            alert("La mois est obligatoire!");
            document.formSecondaire.mois.focus();
            return false;
        }else if(annee == ""){
            alert("L'année est obligatoire!");
            document.formSecondaire.annee.focus();
            return false;
        }else if(discipline == ""){
            alert("Discipline ou spécialisation est obligatoire!");
            document.formSecondaire.discipline.focus();
            return false;
        }else if(institution == ""){
            alert("Institution où vous avez poursuivi vos étude est obligatoire!");
            document.formSecondaire.institution.focus();
            return false;
        }else if(pays == ""){
            alert("Pays est obligatoire!");
            document.formSecondaire.pays.focus();
            return false;
        }
    }

    if(!validerDate(secondaireDebut, secondaireFin)){
        alert("La date de fin doit être supérieure à la date de début");
        document.formSecondaire.secondaireDebut.focus();
        return false;
    }

    var dec = document.formSecondaire.dec.value;
    var decDebut = document.getElementById("decDebut").value;
    var decFin = document.getElementById("decFin").value;
    var decMois = document.getElementById("decMois").value;
    var decAnnee = document.getElementById("decAnnee").value;
    var decDiscipline = document.getElementById("decDiscipline").value;
    var decInstitution = document.getElementById("decInstitution").value;
    var obtenu = document.formSecondaire.obtenu.value;
    if(dec != "" || decDebut != "" || decFin != "" || decMois != "" || decAnnee != "" || decDiscipline != "" || decInstitution != ""
       || obtenu != ""){
        if(dec == ""){
            alert("Autre diplôme de niveau collégial est obligatoire!");
            return false;
        }else if(decDebut == ""){
            alert("Le début d'année est obligatoire!");
            document.formSecondaire.decDebut.focus();
            return false;
        }else if(decFin == ""){
            alert("La fin d'année est obligatoire!");
            document.formSecondaire.decFin.focus();
            return false;
        }else if(decMois == ""){
            alert("La mois est obligatoire!");
            document.formSecondaire.decMois.focus();
            return false;
        }else if(decAnnee == ""){
            alert("Lannée est obligatoire!");
            document.formSecondaire.decAnnee.focus();
            return false;
        }else if(decDiscipline == ""){
            alert("Discipline ou spécialisation est obligatoire!");
            document.formSecondaire.decDiscipline.focus();
            return false;
        }else if(decInstitution == ""){
            alert("Institution où vous avez poursuivi vos études en vue de l’obtention de ce diplôme est obligatoire!");
            document.formSecondaire.decInstitution.focus();
            return false;
        }else if(obtenu == ""){
            alert("Il faut choisir obtenu, à obtenir ou ne sera pas obtenu!");
            return false;
        }
    }

    if(!validerDate(decDebut, decFin)){
        alert("La date de fin doit être supérieure à la date de début");
        document.formSecondaire.decDebut.focus();
        return false;
    }
}


function formReset() {
    document.getElementById("formSecondaire").reset();
}