function validerProgramme() {

    var session = document.formProgramme.session.value;
    if (session == "") {
        alert("La session (Hiver, Été ou Automne) est obligatoire!");
        document.formProgramme.session[0].focus();
        return false;
    }

    var annee = document.getElementById("annee").value;
    if (annee == "") {
        alert("L'année est obligatoire!");
        document.formProgramme.annee.focus();
        return false;
    }

    var titreUn = document.getElementById("titreUn").value;
    if (titreUn == "") {
        alert("Le titre du programme est obligatoire!");
        document.formProgramme.titreUn.focus();
        return false;
    }

    var codeUn = document.getElementById("codeUn").value;
    if (codeUn == "") {
        alert("Le dode du programme est obligatoire!");
        document.formProgramme.codeUn.focus();
        return false;
    }


    var regimeUn = document.formProgramme.regimeUn.value;
    if (regimeUn == "") {
        alert("Le régime est obligatoire!");
        document.getElementsByName("regimeUn")[0].focus();

        return false;
    }


    var typeUn = document.formProgramme.typeUn.value;
    if (typeUn == "") {
        alert("Le type du programme est obligatoire!");
        document.getElementsByName("typeUn")[0].focus();
        return false;
    }

    var titreDeux = document.getElementById("titreDeux").value;
    var codeDeux = document.getElementById("codeDeux").value;
    var regimeDeux = document.formProgramme.regimeDeux.value;
    var typeDeux = document.formProgramme.typeDeux.value;
    if(titreDeux != "" || codeDeux != "" || regimeDeux != "" || typeDeux != ""){
        if(titreDeux == ""){
            alert("Le titre du programme est obligatoire!");
            document.formProgramme.titreDeux.focus();
            return false;
        }else if(codeDeux == ""){
            alert("Le code du programme est obligatoire!");
            document.formProgramme.codeDeux.focus();
            return false;
        }else if(regimeDeux == ""){
            alert("Le regime est obligatoire!");
            document.getElementsByName("regimeDeux")[0].focus();
            return false;
        }else if(typeDeux == ""){
            alert("Le type du programme est obligatoire!");
            document.getElementsByName("typeDeux")[0].focus();
            return false;
        }
    }

    var titreTrois = document.getElementById("titreTrois").value;
    var codeTrois = document.getElementById("codeTrois").value;
    var regimeTrois = document.formProgramme.regimeTrois.value;
    var typeTrois = document.formProgramme.typeTrois.value;
    if(titreTrois != "" || codeTrois != "" || regimeTrois != "" || typeTrois != ""){
        if(titreTrois == ""){
            alert("Le titre du programme est obligatoire!");
            document.formProgramme.titreTrois.focus();
            return false;
        }else if(codeTrois == ""){
            alert("Le code du programme est obligatoire!");
            document.formProgramme.codeTrois.focus();
            return false;
        }else if(regimeTrois == ""){
            alert("Le regime est obligatoire!");
            document.getElementsByName("regimeTrois")[0].focus();
            return false;
        }else if(typeTrois == ""){
            alert("Le type du programme est obligatoire!");
            document.getElementsByName("typeTrois")[0].focus();
            return false;
        }
    }

}

function formReset() {
    document.getElementById("formProgramme").reset();
}
