
function validerDate(debut, fin){
    var debut;
    var fin;
    if(debut > fin){
        return false;
    }else{
        return true;
    }
}

function validerUniversitaire() {

    var nomDiplomeBac = document.getElementById("nomDiplomeBac").value;
    var obtenuBac = document.formUniversitaire.obtenuBac.value;
    var bacDebut = document.getElementById("bacDebut").value;
    var bacFin = document.getElementById("bacFin").value;
    var bacMois = document.getElementById("bacMois").value;
    var bacAnnee = document.getElementById("bacAnnee").value;
    var bacCredit = document.getElementById("bacCredit").value;
    var bacDiscipline = document.getElementById("bacDiscipline").value;
    var bacInstitution = document.getElementById("bacInstitution").value;
    var bacPays = document.getElementById("bacPays").value;

    if(obtenuBac != "" || nomDiplomeBac != "" || bacDebut != "" || bacFin != "" || bacMois != "" || bacAnnee != "" ||bacCredit != ""
       || bacDiscipline != "" || bacInstitution != "" || bacPays != ""){
        if(obtenuBac == ""){
            alert("Diplôme le plus récent entrepris ou complété est obligatoire");
            return false;
        }else if(nomDiplomeBac == ""){
            alert("Le nom du diplôme est obligatoire");
            document.formUniversitaire.nomDiplomeBac.focus();
            return false;
        }else if(bacDebut == ""){
            alert("Le début d'année est obligatoire!");
            document.formUniversitaire.bacDebut.focus();
            return false;
        }else if(bacFin == ""){
            alert("La fin d'année est obligatoire!");
            document.formUniversitaire.bacFin.focus();
            return false;
        }else if(bacMois == ""){
            alert("La mois est obligatoire!");
            document.formUniversitaire.bacMois.focus();
            return false;
        }else if(bacAnnee == ""){
            alert("L'année est obligatoire!");
            document.formUniversitaire.bacAnnee.focus();
            return false;
        }else if(bacCredit == ""){
            alert("Credit est obligatoire!");
            document.formUniversitaire.bacCredit.focus();
            return false;
        }else if(bacDiscipline == ""){
            alert("Discipline est obligatoire!")
            document.formUniversitaire.bacDiscipline.focus();
            return false;
        } else if(bacInstitution == ""){
            alert("Institution où vous avez poursuivi vos étude est obligatoire!");
            document.formUniversitaire.bacInstitution.focus();
            return false;
        }else if(bacPays == ""){
            alert("Pays est obligatoire!");
            document.formUniversitaire.bacPays.focus();
            return false;
        }
    }

    if(!validerDate(bacDebut, bacFin)){
        alert("La date de fin doit être supérieure à la date de début");
        document.formUniversitaire.bacDebut.focus();
        return false;
    }

    var nomDiplomeBacAutre = document.getElementById("nomDiplomeBacAutre").value;
    var obtenuBacAutre = document.formUniversitaire.obtenuBacAutre.value;
    var bacAutreDebut = document.getElementById("bacAutreDebut").value;
    var bacAutreFin = document.getElementById("bacAutreFin").value;
    var bacAutreMois = document.getElementById("bacAutreMois").value;
    var bacAutreAnnee = document.getElementById("bacAutreAnnee").value;
    var bacAutreCredit = document.getElementById("bacAutreCredit").value;
    var bacAutreDiscipline = document.getElementById("bacAutreDiscipline").value;
    var bacAutreInstitution = document.getElementById("bacAutreInstitution").value;
    var bacAutrePays = document.getElementById("bacAutrePays").value;

    if(obtenuBacAutre != "" || nomDiplomeBacAutre != "" || bacAutreDebut != "" || bacAutreFin != "" || bacAutreMois != ""
       || bacAutreAnnee != "" || bacAutreCredit != "" || bacAutreDiscipline != "" || bacAutreInstitution != "" || bacAutrePays != ""){
        if(obtenuBacAutre == ""){
            alert("Autre grade ou diplôme entrepris ou complété est obligatoire");
            return false;
        }else if(nomDiplomeBacAutre == ""){
            alert("Le nom du diplôme est obligatoire");
            document.formUniversitaire.nomDiplomeBacAutre.focus();
            return false;
        } else if(bacAutreDebut == ""){
            alert("Le début d'année est obligatoire!");
            document.formUniversitaire.bacAutreDebut.focus();
            return false;
        }else if(bacAutreFin == ""){
            alert("La fin d'année est obligatoire!");
            document.formUniversitaire.bacAutreFin.focus();
            return false;
        }else if(bacAutreMois == ""){
            alert("La mois est obligatoire!");
            document.formUniversitaire.bacAutreMois.focus();
            return false;
        }else if(bacAutreAnnee == ""){
            alert("L'année est obligatoire!");
            document.formUniversitaire.bacAutreAnnee.focus();
            return false;
        }else if(bacAutreCredit == ""){
            alert("Credit est obligatoire!");
            document.formUniversitaire.bacAutreCredit.focus();
            return false;
        }else if(bacAutreDiscipline == ""){
            alert("Discipline est obligatoire!")
            document.formUniversitaire.bacAutreDiscipline.focus();
            return false;
        } else if(bacAutreInstitution == ""){
            alert("Institution où vous avez poursuivi vos étude est obligatoire!");
            document.formUniversitaire.bacAutreInstitution.focus();
            return false;
        }else if(bacAutrePays == ""){
            alert("Pays est obligatoire!");
            document.formUniversitaire.bacAutrePays.focus();
            return false;
        }
    }

    if(!validerDate(bacAutreDebut, bacAutreFin)){
        alert("La date de fin doit être supérieure à la date de début");
        document.formUniversitaire.bacAutreDebut.focus();
        return false;
    }
}

function formReset() {
    document.getElementById("formUniversitaire").reset();
}