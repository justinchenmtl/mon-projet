
function validerDate(debut, fin){
    var debut;
    var fin;
    if(debut > fin){
        return false;
    }else{
        return true;
    }
}

function validerEmplois() {

    var employeurUn = document.getElementById("employeurUn").value;
    var moisUnDebut = document.getElementById("moisUnDebut").value;
    var moisUnFin = document.getElementById("moisUnFin").value;
    var fonctionUn = document.getElementById("fonctionUn").value;
    var typeUn = document.formEmplois.typeUn.value;
    var tempsUn = document.formEmplois.tempsUn.value;
    if(employeurUn != "" || moisUnDebut != "" || moisUnFin != "" || fonctionUn != "" || typeUn != "" || tempsUn != ""){
        if(employeurUn == ""){
            alert("Nom de l’employeur est obligatoire!");
            document.formEmplois.employeurUn.focus();
            return false;
        }else if(moisUnDebut == ""){
            alert("Le début de la date pendant durée de l’emploi est obligatoire!");
            document.formEmplois.moisUnDebut.focus();
            return false;
        }else if(new Date(moisUnDebut).getDate() != moisUnDebut.substring(moisUnDebut.length - 2)){
            alert("Le début d'année pendant premier emploi est invalide!");
            document.formEmplois.moisUnDebut.focus();
            return false;
        }else if(moisUnFin == ""){
            alert("La fin de la date pendant durée de l’emploi est obligatoire!");
            document.formEmplois.moisUnFin.focus();
            return false;
        }else if(new Date(moisUnFin).getDate() != moisUnFin.substring(moisUnFin.length - 2)){
            alert("La fin d'année pendant premier emploi est invalide!");
            document.formEmplois.moisUnFin.focus();
            return false;
        }else if(fonctionUn == ""){
            alert("La fonction occupée est obligatoire!");
            document.formEmplois.fonctionUn.focus();
            return false;
        }else if(typeUn == ""){
            alert("Le type d'emploi est obligatoire!");
            document.getElementsByName("typeUn")[0].focus();
            return false;
        }else if(tempsUn == ""){
            alert("Le temps complet ou partiel est obligatoire!");
            document.getElementsByName("tempsUn")[0].focus();
            return false;
        }
    }

    if(!validerDate(new Date(moisUnDebut), new Date(moisUnFin))){
        alert("la date de fin doit être supérieure à la date de début");
        document.formEmplois.moisUnDebut.focus();
        return false;
    }

    var employeurDeux = document.getElementById("employeurDeux").value;
    var moisDeuxDebut = document.getElementById("moisDeuxDebut").value;
    var moisDeuxFin = document.getElementById("moisDeuxFin").value;
    var fonctionDeux = document.getElementById("fonctionDeux").value;
    var typeDeux = document.formEmplois.typeDeux.value;
    var tempsDeux = document.formEmplois.tempsDeux.value;
    if(employeurDeux != "" || moisDeuxDebut != "" || moisDeuxFin != "" || fonctionDeux != "" || typeDeux != "" || tempsDeux != ""){
        if(employeurDeux == ""){
            alert("Nom de l'employeur est obligatoire");
            document.formEmplois.employeurDeux.focus();
            return false;
        }else if(moisDeuxDebut == ""){
            alert("Le début de la date pendant durée de l’emploi est obligatoire");
            document.formEmplois.moisDeuxDebut.focus();
            return false;
        }else if(new Date(moisDeuxDebut).getDate() != moisDeuxDebut.substring(moisDeuxDebut.length - 2)){
            alert("Le début d'année pendant deuxième emploi est invalide!");
            document.formEmplois.moisDeuxDebut.focus();
            return false;
        }else if(moisDeuxFin == ""){
            alert("La fin de la date pendant durée de l’emploi est obligatoire!");
            document.formEmplois.moisDeuxFin.focus();
            return false;
        }else if(new Date(moisDeuxFin).getDate() != moisDeuxFin.substring(moisDeuxFin.length - 2)){
            alert("Le début d'année pendant deuxième emploi est invalide!");
            document.formEmplois.moisDeuxFin.focus();
            return false;
        }else if(fonctionDeux == ""){
            alert("La fonction occupée est obligatoire!");
            document.formEmplois.fonctionDeux.focus();
            return false;
        }else if(typeDeux == ""){
            alert("Le type d'emploi est obligatoire!");
            document.getElementsByName("typeDeux")[0].focus();
            return false;
        }else if(tempsDeux == ""){
            alert("Le temps complet ou partiel est obligatoire!");
            document.getElementsByName("tempsDeux")[0].focus();
            return false;
        }
    }

    if(!validerDate(new Date(moisDeuxDebut), new Date(moisDeuxFin))){
        alert("la date de fin doit être supérieure à la date de début");
        document.formEmplois.moisDeuxDebut.focus();
        return false;
    }

    var employeurTrois = document.getElementById("employeurTrois").value;
    var moisTroisDebut = document.getElementById("moisTroisDebut").value;
    var moisTroisFin = document.getElementById("moisTroisFin").value;
    var fonctionTrois = document.getElementById("fonctionTrois").value;
    var typeTrois = document.formEmplois.typeTrois.value;
    var tempsTrois = document.formEmplois.tempsTrois.value;
    if(employeurTrois != "" || moisTroisDebut!= "" || moisTroisFin != "" || fonctionTrois != "" || typeTrois != "" || tempsTrois != ""){
        if(employeurTrois == ""){
            alert("Nom de l'employeur est obligatoire");
            document.formEmplois.employeurTrois.focus();
            return false;
        }else if(moisTroisDebut == ""){
            alert("Le début de la date pendant durée de l’emploi est obligatoire");
            document.formEmplois.moisTroisDebut.focus();
            return false;
        }else if(new Date(moisTroisDebut).getDate() != moisTroisDebut.substring(moisTroisDebut.length - 2)){
            alert("Le début d'année pendant troisième emploi est invalide!");
            document.formEmplois.moisTroisDebut.focus();
            return false;
        }else if(moisTroisFin == ""){
            alert("La fin de la date pendant durée de l’emploi est obligatoire!");
            document.formEmplois.moisTroisFin.focus();
            return false;
        }else if(new Date(moisTroisFin).getDate() != moisTroisFin.substring(moisTroisFin.length - 2)){
            alert("La fin d'année pendant troisième emploi est invalide!");
            document.formEmplois.moisTroisFin.focus();
            return false;
        }else if(fonctionTrois == ""){
            alert("La fonction occupée est obligatoire!");
            document.formEmplois.fonctionTrois.focus();
            return false;
        }else if(typeTrois == ""){
            alert("Le type d'emploi est obligatoire!");
            document.getElementsByName("typeTrois")[0].focus();
            return false;
        }else if(tempsTrois == ""){
            alert("Le temps complet ou partiel est obligatoire!");
            document.getElementsByName("tempsTrois")[0].focus();
            return false;
        }
    }
    if(!validerDate(new Date(moisTroisDebut), new Date(moisTroisFin))){
        alert("la date de fin doit être supérieure à la date de début");
        document.formEmplois.moisTroisDebut.focus();
        return false;
    }


}

function formReset() {
    document.getElementById("formEmplois").reset();
}