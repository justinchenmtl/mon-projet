
function afficherPere(){
    var nomPere = document.getElementById("nomPere");
    var prenomPere = document.getElementById("prenomPere");
    var nomPereHide = document.getElementById("nomPereHide");
    var prenomPereHide = document.getElementById("prenomPereHide");

    if (nomPere.style.display == "none" || prenomPere.style.display == "none" || nomPereHide.style.display == "none"
        || prenomPereHide.style.display == "none") {
        nomPere.style.display = "block";
        prenomPere.style.display = "block";
        nomPereHide.style.display = "block";
        prenomPereHide.style.display = "block";

    }else {
        nomPere.style.display = "none";
        prenomPere.style.display = "none";
        nomPereHide.style.display = "none";
        prenomPereHide.style.display = "none";
    }
}

function afficherMere(){
    var nomMere = document.getElementById("nomMere");
    var prenomMere = document.getElementById("prenomMere");
    var nomMereHide = document.getElementById("nomMereHide");
    var prenomMereHide = document.getElementById("prenomMereHide");

    if (nomMere.style.display == "none" || prenomMere.style.display == "none" || nomMereHide.style.display == "none"
        || prenomMereHide.style.display == "none") {
        nomMere.style.display = "block";
        prenomMere.style.display = "block";
        nomMereHide.style.display = "block";
        prenomMereHide.style.display = "block";
    } else {
        nomMere.style.display = "none";
        prenomMere.style.display = "none";
        nomMereHide.style.display = "none";
        prenomMereHide.style.display = "none";
    }
}

function validerIdentification(){

    var nomFamille = document.getElementById("nomFamille").value;
    if(nomFamille == "") {
        alert("Le nom de famille à la naissance est obligatoire!");
        document.form_ident.nomFamille.focus();
        return false;
    }

    var prenom = document.getElementById("prenom").value;
    if(prenom == ""){
        alert("Le prenom usuel est obligatoire!");
        document.form_ident.prenom.focus();
        return false;
    }

    var dateNe = document.getElementById("dateNe").value;
    if(dateNe == ""){
        alert("La date de naissance est obligatoire!");
        document.form_ident.dateNe.focus();
        return false;
    }else {
        if (new Date(dateNe).getDate() != dateNe.substring(dateNe.length - 2)) {
            alert("La date de naissance est invalide!");
            document.form_ident.dateNe.focus();
            return false;
        }
    }

    var sex = document.form_ident.sex.value;
    if(sex == ""){
        alert("Le sexe est obligatoire");
        document.getElementsByName("sex")[0].focus();
        return false;
    }

    var cpMinistere = document.getElementById("cpMinistere").value;
    const regexCP = /^[a-zA-Z]{4}[0-9]{8}$/;
    if(cpMinistere == ""){
        alert("Le code permanent du ministère est obligatoire!");
        document.form_ident.cpMinistere.focus();
        return false;
    }
    if(cpMinistere.length != 12 || !regexCP.test(cpMinistere)){
        alert("Le code permanent du ministère est invalid (Les codes permanents ont le format XXXX99999999)!");
        document.form_ident.cpMinistere.focus();
        return false;
    }

    var cpUQAM = document.getElementById("cpUQAM").value;
    if(!regexCP.test(cpUQAM) && cpUQAM != ""){
        alert("le code permanent de UQAM est invalid (Les codes permanents ont le format XXXX99999999) !");
        document.form_ident.cpUQAM.focus();
        return false;
    }

    var citoyenne = document.form_ident.citoyenne.value;
    if(citoyenne == ""){
        alert("Le citoyenneté est obligatoire!");
        document.getElementsByName("citoyenne")[0].focus();
        return false;
    }
    if(citoyenne == "Autre") {
        var citoyennePrecise = document.getElementById("citoyennePrecise").value;
        if (citoyennePrecise == "") {
            alert("Precise de citoyenneté est obligatoire");
            document.getElementById("citoyennePrecise").focus();
            return false;
        }
    }

    var statut = document.form_ident.statut.value;
    if(statut == ""){
        alert("Le statut au Canada est obligatoire!");
        document.getElementsByName("statut")[0].focus();
        return false;
    }

    var pere = document.getElementsByName("pere");
    var nomPere = document.getElementById("nomPere").value;
    var prenomPere = document.getElementById("prenomPere").value;
    for(var j=0; j<pere.length; j++) {
        if (pere[j].checked == true) {
            if (nomPere == "") {
                alert("Le nom du Père est obligatoire!");
                document.getElementById("nomPere").focus();
                return false;
            } else if (prenomPere == "") {
                alert("Le prenom du Père est obligatoire!");
                document.getElementById("prenomPere").focus();
                return false;
            }
        }
    }

    var mere = document.getElementsByName("mere");
    var nomMere = document.getElementById("nomMere").value;
    var prenomMere = document.getElementById("prenomMere").value;
    for(var i=0; i<mere.length; i++){
        if (mere[i].checked  == true) {
            if (nomMere == "") {
                alert("Le nom du Mère est obligatoire!");
                document.getElementById("nomMere").focus();
                return false;
            } else if (prenomMere == "") {
                alert("Le prenom du Mère est obligatoire!");
                document.getElementById("prenomMere").focus();
                return false;
            }
        }
    }

    var nas = document.getElementById("nas").value;
    const regexNAS = /^[0-9]{9}$/;
    if(!regexNAS.test(nas) && nas != ""){
        alert("No d’assurance sociale est invalid (Le numéro d'assurance social est composé de 9 chiffres)!")
        document.form_ident.nas.focus();
        return false;
    }

    var lieu = document.getElementById("lieu").value;
    if(lieu == ""){
        alert("Le lieu de la naissance est obligatoire!");
        document.form_ident.lieu.focus();
        return false;
    }

    var langueUsage = document.form_ident.langueUsage.value;
    if(langueUsage == ""){
        alert("La langue d’usage est obligatoire!");
        document.getElementsByName("langueUsage")[0].focus();
        return false;
    }
    if(langueUsage == "Autre") {
        var usagePrecise = document.getElementById("usagePrecise").value;
        if (usagePrecise == "") {
            alert("Precise de la langue d'usage est obligatoire");
            document.getElementById("usagePrecise").focus();
            return false;
        }
    }

    var langueMaternelle = document.form_ident.langueMaternelle.value;
    if(langueMaternelle == ""){
        alert("La langue maternelle est obligatoire!");
        document.getElementsByName("langueMaternelle")[0].focus();
        return false;
    }
    if(langueMaternelle == "Autre") {
        var maternellePrecise = document.getElementById("maternellePrecise").value;
        if (maternellePrecise == "") {
            alert("Precise de la langue maternelle est obligatoire");
            document.getElementById("maternellePrecise").focus();
            return false;
        }
    }

    var cellulaire = document.getElementById("cellulaire").value;
    const regexTele = /[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]/;
    if(cellulaire == ""){
        alert("Le cellulaire est obligatoire!");
        document.form_ident.cellulaire.focus();
        return false;
    }
    if(!regexTele.test(cellulaire)){
        alert("Le cellulaire est invalid (Les numéros de téléphones ont le format 999-999-9999) !");
        document.form_ident.cellulaire.focus();
        return false;
    }

    var teleTravail = document.getElementById("teleTravail").value;
    if(!regexTele.test(teleTravail) && teleTravail != ""){
        alert("Téléphone au travail est invalid (Les numéros de téléphones ont le format 999-999-9999) !");
        document.form_ident.teleTravail.focus();
        return false;
    }

    var teleDomicile = document.getElementById("teleDomicile").value;
    if(!regexTele.test(teleDomicile) && teleDomicile != ""){
        alert("Téléphone à domicile est invalid (Les numéros de téléphones ont le format 999-999-9999) !");
        document.form_ident.teleDomicile.focus();
        return false;
    }

    var courriel = document.getElementById("courriel").value;
    const regexEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(courriel != "" && !regexEmail.test(courriel)){
        alert("Le courriel doit avoir un format valide!");
        document.form_ident.courriel.focus();
        return false;

    }

    var adresse = document.getElementById("adresse").value;
    if(adresse == ""){
        alert("L'adresse de correspondance est obligatoire!");
        document.form_ident.adresse.focus();
        return false;
    }

    var cpCorrespond = document.getElementById("cpCorrespond").value;
    const regexCodePostal = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
    if(cpCorrespond != "" && !regexCodePostal.test(cpCorrespond)) {
        alert("Le code postal est invalid (Les codes postaux ont le format X9X 9X9) !");
        document.form_ident.cpCorrespond.focus();
        return false;
    }

    var cpDiff = document.getElementById("cpDiff").value;
    if(!regexCodePostal.test(cpDiff) && cpDiff != ""){
        alert("Le code postal est invalid (Les codes postaux ont le format X9X 9X9) !");
        document.form_ident.cpDiff.focus();
        return false;
    }
}


function formReset() {
    document.getElementById("form_ident").reset();
}







