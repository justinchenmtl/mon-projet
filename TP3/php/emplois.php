<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $employeurUn = $_POST['employeurUn'];
        $moisUnDebut = $_POST['moisUnDebut'];
        $moisUnFin = $_POST['moisUnFin'];
        $fonctionUn = $_POST['fonctionUn'];
        $typeUn = $_POST['typeUn'];
        $tempsUn = $_POST['tempsUn'];
        $employeurDeux = $_POST['employeurDeux'];
        $moisDeuxDebut = $_POST['moisDeuxDebut'];
        $moisDeuxFin = $_POST['moisDeuxFin'];
        $fonctionUnDeux = $_POST['fonctionDeux'];
        $typeDeux = $_POST['typeDeux'];
        $tempsDeux = $_POST['tempsDeux'];
        $employeurTrois = $_POST['employeurTrois'];
        $moisTroisDebut = $_POST['moisTroisDebut'];
        $moisTroisFin = $_POST['moisTroisFin'];
        $fonctionTrois = $_POST['fonctionTrois'];
        $typeTrois = $_POST['typeTrois'];
        $tempsTrois = $_POST['tempsTrois'];
        $renseignement = $_POST['renseignement'];

    if ($moisUnDebut>$moisUnFin || $moisDeuxDebut>$moisDeuxFin || $moisTroisDebut>$moisTroisFin){
        $dateErr = "la date de fin doit être supérieure à la date de début!";
    }

    if($employeurUn != "") {
        if (empty($_POST['moisUnDebut'])) {
            $moisUnDebutErr = "Le début de la date pendant durée de l’emploi est obligatoire";
        } else if (empty($_POST['moisUnFin'])) {
            $moisUnFinErr = "La fin de la date pendant durée de l’emploi est obligatoire!";
        } else if (empty($_POST['fonctionUn'])) {
            $fonctionUnErr = "La fonction occupée est obligatoire!";
        } else if (empty($_POST['typeUn'])) {
            $typeUnErr = "Le type d'emploi est obligatoire!";
        } else if (empty($_POST['tempsUn'])) {
            $tempsUnErr = "Le temps complet ou partiel est obligatoire!";
        }
    }

    if($employeurDeux != "") {
        if (empty($_POST['moisDeuxDebut'])) {
            $moisDeuxDebutErr = "Le début de la date pendant durée de l’emploi est obligatoire";
        } else if (empty($_POST['moisDeuxFin'])) {
            $moisDeuxFinErr = "La fin de la date pendant durée de l’emploi est obligatoire!";
        } else if (empty($_POST['fonctionDeux'])) {
            $fonctionDeuxErr = "La fonction occupée est obligatoire!";
        } else if (empty($_POST['typeDeux'])) {
            $typeDeuxErr = "Le type d'emploi est obligatoire!";
        } else if (empty($_POST['tempsDeux'])) {
            $tempsDeuxErr = "Le temps complet ou partiel est obligatoire!";
        }
    }

    if($employeurTrois != "") {
        if (empty($_POST['moisTroisDebut'])) {
            $moisTroisDebutErr = "Le début de la date pendant durée de l’emploi est obligatoire";
        } else if (empty($_POST['moisTroisFin'])) {
            $moisTroisFinErr = "La fin de la date pendant durée de l’emploi est obligatoire!";
        } else if (empty($_POST['fonctionTrois'])) {
            $fonctionTroisErr = "La fonction occupée est obligatoire!";
        } else if (empty($_POST['typeTrois'])) {
            $typeTroisErr = "Le type d'emploi est obligatoire!";
        } else if (empty($_POST['tempsTrois'])) {
            $tempsTroisErr = "Le temps complet ou partiel est obligatoire!";
        }
    }

    $cpMinistere = $_COOKIE['myCookie'];
    if (empty($moisUnDebutErr) && empty($moisUnFinErr) && empty($fonctionUnErr) && empty($typeUnErr) && empty($tempsUnErr) && empty($dateErr)
        && empty($moisDeuxDebutErr) && empty($moisDeuxFinErr) && empty($fonctionDeuxErr) && empty($typeDeuxErr) && empty($tempsDeuxErr)
        && empty($moisTroisDebutErr) && empty($moisTroisFinErr) && empty($fonctionTroisErr) && empty($typeTroisErr) && empty($tempsTroisErr)) {
        header("Location: confirmation.php", true, 303);
        $myfile = fopen("../$cpMinistere.txt", "a+") or die("unable to open file!");
        $temp = "\n" . " Nom de premier employeur: " . $employeurUn . ", Début d'emploi: " . $moisUnDebut . ", Fin d'emploi: " . $moisUnFin.
            ", La fonction occupée: " . $fonctionUn . ", Le type d'emploi: " . $typeUn . ", Le temps d'emploi: " . $tempsUn.
            ", Nom de Deuxième employeur: " . $employeurDeux . ", Debut d'emploi: " . $moisDeuxDebut . ", Fin d'emploi: " .
            $moisDeuxFin . ", La fonction occupee: " . $fonctionUnDeux . ", Le type d'emploi: " . $typeDeux . ", Le temps d'emploi: " .
            $tempsDeux . ", Nom de troisième employeur: " . $employeurTrois . ", Début d'emploi: " . $moisTroisDebut . ", Fin d'emploi: " . $moisTroisFin . ", La fonction occupée: " .
            $fonctionTrois . ", Le type d'emploi: " . $typeTrois. ", Le temps d'emploi: " . $tempsTrois . ", Renseignement supplementaire: " . $renseignement . "\n";
        $text = utf8_encode($temp);
        $text = "\xEF\xBB\xBF".$temp;
        fwrite($myfile, $text);
        fclose($myfile);
        exit;
    }else{
        header("location: erreur.php", true, 400);
        exit;
    }
}
?>