<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Confirmation page</title>
            <link rel="stylesheet" href="../css/corp.css"/>
        </head>

        <body class="fr">
            <link rel="stylesheet" href="../css/tete.css"/>
            <link rel="stylesheet" href="../css/pied.css"/>

            <div class="section_top" id="top">
                <a class="web_logo" href="../inscription.html">
                    <img src="../css/img/uqam-logo.jpg" alt="UQAM Logo"/>
                </a>
                <p>Admission d'étude à l'UQAM</p>
            </div>
            <div class="quitter">
                <a href="">Aide |</a>
                <a href="">Quitter</a>

            </div>

            <div class="head" id="head">
                <p style="text-align: right">
                    Demande d'admission aux études de premier cycle
                </p>
                <hr>
                <h2 style="text-align: center; color: blue">
                    Votre inscription a réussi! <br>
                    Permettez-nous de vous remercier de votre intérêt pour l’UQAM.<br>
                </h2>
                <h5 style="text-align: center; color: blue">
                    (Les modalités sont expliquées dans un document (autorisation d'inscription) que vous recevez<br>
                    par courrier, quelques semaines après la confirmation de son admission.)
                </h5>
                <hr>
            </div>



        </body>
    </html>