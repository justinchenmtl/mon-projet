<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $nomFamille = $_POST['nomFamille'];
        $prenom = $_POST['prenom'];
        $dateNe = $_POST['dateNe'];
        $sex = $_POST['sex'];
        $cpMinistere = $_POST['cpMinistere'];
        $cpUQAM = $_POST['cpUQAM'];
        $citoyenne = $_POST['citoyenne'];
        $citoyennePrecise = $_POST['citoyennePrecise'];
        $statut = $_POST['statut'];
        $nas = $_POST['nas'];
        $lieu = $_POST['lieu'];
        $nomPere = $_POST['nomPere'];
        $prenomPere = $_POST['prenomPere'];
        $nomMere = $_POST['nomMere'];
        $prenomMere = $_POST['prenomMere'];
        $langueUsage = $_POST['langueUsage'];
        $usagePrecise = $_POST['usagePrecise'];
        $langueMaternelle = $_POST['langueMaternelle'];
        $maternellePrecise = $_POST['maternellePrecise'];
        $cellulaire = $_POST['cellulaire'];
        $teleTravail = $_POST['teleTravail'];
        $teleDomicile = $_POST['teleDomicile'];
        $courriel = $_POST['courriel'];
        $adresse = $_POST['adresse'];
        $cpCorrespond = $_POST['cpCorrespond'];
        $adrDiff = $_POST['adrDiff'];
        $cpDiff = $_POST['cpDiff'];


    if (empty($_POST["nomFamille"])) {
        $nomFamilleErr = "Le nom de famille à la naissance est obligatoire!";
    }

    if (empty($_POST["prenom"])) {
        $prenomErr = "Le prenom usuel est obligatoire!";
    }

    if (empty($_POST["dateNe"])) {
        $dateNeErr = "La date de naissance est obligatoire!";
    } else if(date('Y-m-d', strtotime($dateNe)) == $dateNe){
        $dateNeErr = "La date de naissance est invalide!";
    }

    if (empty($_POST["sex"])) {
        $sexErr = "Le sexe est obligatoire!";
    }


    if (empty($_POST["cpMinistere"])) {
        $cpMinistereErr = "Le code permanent du ministère est obligatoire!";
    }else if((!preg_match("/^[a-zA-Z]{4}[0-9]{8}$/", $cpMinistere)) || strlen($cpMinistere) != 12){
        $cpMinistereErr = "Le code permanent du ministère est invalid (Les codes permanents ont le format XXXX99999999)!";
    }

    if (empty($_POST["citoyenne"])) {
        $citoyenneErr = "Le citoyenneté est obligatoire!";
    }

    if (empty($_POST["statut"])) {
        $statutErr = "Le statut au Canada est obligatoire!";
    }

    if(!empty($nas) && !preg_match("/^[0-9]{9}$/", $nas)){
        $nasErr = "No d’assurance sociale est invalid (Le numéro d'assurance social est composé de 9 chiffres)!";
    }

    if (empty($_POST["lieu"])) {
        $lieuErr = "Le lieu de la naissance est obligatoire!";
    }

    if (empty($_POST["langueUsage"])) {
        $langueUsageErr = "La langue d’usage est obligatoire!";
    }

    if (empty($_POST["langueMaternelle"])) {
        $langueMaternelleErr = "La langue maternelle est obligatoire!";
    }

    if (empty($_POST["cellulaire"])) {
        $cellulaireErr = "Le cellulaire est obligatoire!";
    }else if(!empty($cellulaire) && !preg_match("/[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]/", $cellulaire)){
        $cellulaireErr = "Le cellulaire est invalid (Les numéros de téléphones ont le format 999-999-9999)!";
    }

    if(!empty($teleTravail) && !preg_match("/[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]/", $teleTravail)){
        $teleTravailErr = "Téléphone au travail est invalid (Les numéros de téléphones ont le format 999-999-9999)!";
    }

    if(!empty($teleDomicile) && !preg_match("/[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]/", $teleDomicile)){
        $teleDomicileErr = "Téléphone à domicile est invalid (Les numéros de téléphones ont le format 999-999-9999)!";
    }

    if (!empty($courriel) && !preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $courriel)){
        $courrielErr = "Le courriel doit avoir un format valide!";
    }

    if (empty($_POST["adresse"])) {
        $adresseErr = "L'adresse de correspondance est obligatoire!";
    }

    if (!empty($cpCorrespond) && !preg_match("/^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/", $cpCorrespond)){
        $cpCorrespondErr = "Le code postal est invalid (Les codes postaux ont le format X9X 9X9)!";
    }

    if (!empty($cpDiff) && !preg_match("/^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/", $cpDiff)){
        $cpDiffErr = "Le code postal est invalid (Les codes postaux ont le format X9X 9X9)!";
    }

    setcookie('myCookie', $cpMinistere);

    if (empty($nomFamilleErr) && empty($prenomErrErr) && empty($dateNeErr) && empty($sexErr) && empty($cpMinistereErr)
        && empty($citoyenneErr) && empty($statutErr) && empty($lieuErr) && empty($langueUsageErr) && empty($langueMaternelleErr)
        && empty($cellulaireErr) && empty($teleTravailErr) && empty($teleDomicileErr) && empty($adresseErr) && empty($nasErr)
        && empty($courrielErr) && empty($cpCorrespondErr) && empty($cpDiffErr)) {
        header("Location: ../programme.html", true, 303);
        $myfile = fopen("../$cpMinistere.txt", "a+") or die("unable to open file!");
        $temp = "\n" . "Nom de famille: " . $nomFamille . ", Prénom: " . $prenom . ", Date de naissance: " . $dateNe . ", Sexe: " . $sex .
            ", Code permanent du ministère: " . $cpMinistere . ", Code UQAM: " . $cpUQAM. ", Citoyenneté: " . $citoyenne .
            ",  Statut au Canada: " . $statut . ", Lieu de naissance: " . $lieu . ", No d'assuranc sociale: " . $nas .
            ", Nom du Pere: " . $nomPere . ", Prenom du Pere: " . $prenomPere . ", Nom du Mere: " . $nomMere .
            ", Prenom du Mere: " . $prenomMere . ", Langue d'usage: " . $langueUsage . ", Langue maternelle: " . $langueMaternelle .
            ", Cellulaire: " . $cellulaire . ", Telephone au travail: " . $teleTravail . ", Telephone domicile: " .$teleDomicile .
            ", Courriel: " . $courriel . ", Adresse de correspondance: " . $adresse . ", Code postal: " . $cpCorrespond .
            ", Adresse si differente de correspondance: " . $adrDiff . ", Code postal: " . $cpDiff .  "\n";
        $text = utf8_encode($temp);
        $text = "\xEF\xBB\xBF".$temp;
        fwrite($myfile, $text);
        fclose($myfile);
        exit;
    }else{
        header("location: erreur.php", true, 400);
        exit;
    }
    }
?>