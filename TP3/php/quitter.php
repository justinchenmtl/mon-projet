<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Quitter</title>
            <link rel="stylesheet" href="../css/corp.css"/>
        </head>

        <body class="fr">
            <link rel="stylesheet" href="../css/tete.css"/>
            <link rel="stylesheet" href="../css/pied.css"/>

            <div class="section_top" id="top">
                <a class="web_logo" href="../inscription.html">
                    <img src="../css/img/uqam-logo.jpg" alt="UQAM Logo"/>
                </a>
                <p>Admission d'étude à l'UQAM</p>
            </div>
            <div class="quitter">
                <a href="https://vie-etudiante.uqam.ca/conseils-soutien/psycho/aide.html">Aide |</a>
                <a href="quitter.php">Quitter</a>

            </div>

            <div class="head" id="head">
                <p style="text-align: right">
                    Demande d'admission aux études de premier cycle
                </p>
                <hr>
                <h2 style="text-align: center; color: blue">
                    <input type="button" value="Démarrer une nouvelle session" onclick="location.href='../inscription.html'"> <br>
                </h2>
                <h4 style="text-align: center; color: blue">
                    Vous venez de quitter le système Répertoire institutionnel des registraire.<br>
                    Pour vous assurer de la confidentialité des transactions effectuées, quittez le navigateur.
                </h4>
                <hr>
            </div>



        </body>
    </html>