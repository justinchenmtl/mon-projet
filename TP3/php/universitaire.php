<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $obtenuBac = $_POST['obtenuBac'];
        $nomDiplomeBac = $_POST['nomDiplomeBac'];
        $bacDebut = $_POST['bacDebut'];
        $bacFin = $_POST['bacFin'];
        $bacMois = $_POST['bacMois'];
        $bacAnnee = $_POST['bacAnnee'];
        $bacCredit = $_POST['bacCredit'];
        $bacDiscipline = $_POST['bacDiscipline'];
        $bacInstitution = $_POST['bacInstitution'];
        $bacPays = $_POST['bacPays'];
        $obtenuBacAutre = $_POST['obtenuBacAutre'];
        $nomDiplomeBacAutre = $_POST['nomDiplomeBacAutre'];
        $bacAutreDebut = $_POST['bacAutreDebut'];
        $bacAutreFin = $_POST['bacAutreFin'];
        $bacAutreMois = $_POST['bacAutreMois'];
        $bacAutreAnnee = $_POST['bacAutreAnnee'];
        $bacAutreCredit = $_POST['bacAutreCredit'];
        $bacAutreDiscipline = $_POST['bacAutreDiscipline'];
        $bacAutreInstitution = $_POST['bacAutreInstitution'];
        $bacAutrePays = $_POST['bacAutrePays'];

    if ($bacDebut>$bacFin || $bacAutreDebut>$bacAutreFin){
        $dateErr = "la date de fin doit être supérieure à la date de début!";
    }

    if($obtenuBac != "" || $nomDiplomeBac != ""){
        if(empty($_POST['nomDiplomeBac'])){
            $nomDiplomeBacErr = "Le nom du diplôme est obligatoire";
        }else if(empty($_POST['bacDebut'])){
            $bacDebutErr = "Le début d'année est obligatoire!";
        }else if(empty($_POST['bacFin'])){
            $bacFinErr = "La fin d'année est obligatoire!";
        }else if(empty($_POST['bacMois'])){
            $bacMoisErr = "La mois est obligatoire!";
        }else if(empty($_POST['bacAnnee'])){
            $bacAnneeErr = "L'année est obligatoire!";
        }else if(empty($_POST['bacCredit'])){
            $bacCreditErr = "Credit est obligatoire!";
        }else if(empty($_POST['bacDiscipline'])){
            $bacDisciplineErr = "Discipline est obligatoire!";
        } else if(empty($_POST['bacInstitution'])){
            $bacInstitutionErr = "Institution où vous avez poursuivi vos étude est obligatoire!";
        }else if(empty($_POST['bacPays'])){
            $bacPaysErr = "Pays est obligatoire!";
        }
    }

    if($obtenuBacAutre != "" || $nomDiplomeBacAutre != ""){
        if(empty($_POST['nomDiplomeBacAutre'])){
            $nomDiplomeBacAutreErr = "Le nom du diplôme est obligatoire";
        }else if(empty($_POST['bacAutreDebut'])){
            $bacAutreDebutErr = "Le début d'année est obligatoire!";
        }else if(empty($_POST['bacAutreFin'])){
            $bacAutreFinErr = "La fin d'année est obligatoire!";
        }else if(empty($_POST['bacAutreMois'])){
            $bacAutreMoisErr = "La mois est obligatoire!";
        }else if(empty($_POST['bacAutreAnnee'])){
            $bacAutreAnneeErr = "L'année est obligatoire!";
        }else if(empty($_POST['bacAutreCredit'])){
            $bacAutreCreditErr = "Credit est obligatoire!";
        }else if(empty($_POST['bacAutreDiscipline'])){
            $bacAutreDisciplineErr = "Discipline est obligatoire!";
        } else if(empty($_POST['bacAutreInstitution'])){
            $bacAutreInstitutionErr = "Institution où vous avez poursuivi vos étude est obligatoire!";
        }else if(empty($_POST['bacAutrePays'])){
            $bacAutrePaysErr = "Pays est obligatoire!";
        }
    }



    $cpMinistere = $_COOKIE['myCookie'];
    if (empty($nomDiplomeBacErr) && empty($bacDebutErr) && empty($bacFinErr) && empty($bacMoisErr) && empty($bacAnneeErr)
        && empty($bacCreditErr) && empty($bacDisciplineErr) && empty($bacInstitutionErr) && empty($bacPaysErr) && empty($nomDiplomeBacAutreErr)
        && empty($bacAutreDebutErr) && empty($bacAutreFinErr) && empty($bacAutreMoisErr) && empty($bacAutreAnneeErr) && empty($bacAutreCreditErr)
        && empty($bacAutreDisciplineErr) && empty($bacAutreInstitutionErr) && empty($bacAutrePaysErr) && empty($dateErr)) {
        header("Location: ../emplois.html", true, 303);
        $myfile = fopen("../$cpMinistere.txt", "a+") or die("unable to open file!");
        $temp = "\n" . " Diplôme le plus recent entrepris: " . $obtenuBac . ", Nom du diplome: " . $nomDiplomeBac . ", Début d'annee: " . $bacDebut.
            ", La fin d'annee: " . $bacFin . ", Mois d'obtension: " . $bacMois . ", Annee d'obtension: " . $bacAnnee.
            ", Nombre de credits: " . $bacCredit . ", Discipline: " . $bacDiscipline . ", Institution: " .
            $bacInstitution . ", Pays: " . $bacPays . ", Autre diplome entrepris: " . $obtenuBacAutre . ", Nom du diplôme: " .
            $nomDiplomeBacAutre . ", Debut d'annee: " . $bacAutreDebut . ", Fin d'annee: " . $bacAutreFin . ", Mois d'obtention: " . $bacAutreMois . ", L'année d'obtension: " .
            $bacAutreAnnee . ", Nombre de credits: " . $bacAutreCredit. ", Discipline: " . $bacAutreDiscipline . ", Institution: " . $bacAutreInstitution .
            ", Pays: " . $bacAutrePays . "\n";
        $text = utf8_encode($temp);
        $text = "\xEF\xBB\xBF".$temp;
        fwrite($myfile, $text);
        fclose($myfile);
        exit;
    }else{
        header("location: erreur.php", true, 400);
        exit;
    }

    }
?>