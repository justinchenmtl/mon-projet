<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $session = $_POST['session'];
        $annee = $_POST['annee'];
        $titreUn = $_POST['titreUn'];
        $codeUn = $_POST['codeUn'];
        $regimeUn = $_POST['regimeUn'];
        $typeUn = $_POST['typeUn'];
        $titreDeux = $_POST['titreDeux'];
        $codeDeux = $_POST['codeDeux'];
        $regimeDeux = $_POST['regimeDeux'];
        $typeDeux = $_POST['typeDeux'];
        $titreTrois = $_POST['titreTrois'];
        $codeTrois = $_POST['codeTrois'];
        $regimeTrois = $_POST['regimeTrois'];
        $typeTrois = $_POST['typeTrois'];


    if (empty($_POST["session"])) {
        $sessionErr = "La session (Hiver, Été ou Automne) est obligatoire!";
    }

    if (empty($_POST["annee"])) {
        $anneeErr = "L'année est obligatoire!";
    }

    if (empty($_POST["titreUn"])) {
        $titreUnErr = "Le titre du programme est obligatoire!";
    }

    if (empty($_POST["codeUn"])) {
        $codeUnErr = "Le dode du programme est obligatoire!";
    }

    if (empty($_POST["regimeUn"])) {
        $regimeUnErr = "Le régime est obligatoire!";
    }

    if (empty($_POST["typeUn"])) {
        $typeUnErr = "Le type du programme est obligatoire!";
    }

    if ($titreDeux != "" || $codeDeux != "") {
        if(empty($_POST['titreDeux'])){
            $titreDeuxErr = "Le titre du programme est obligatoire!";
        }else if(empty($_POST['codeDeux'])){
            $codeDeuxErr = "Le code du programme est obligatoire!";
        }else if(empty($_POST['regimeDeux'])){
            $regimeDeuxErr = "Le regime est obligatoire!";
        }else if(empty($_POST['typeDeux'])){
            $typeDeuxErr = "Le type du programme est obligatoire!";
        }
    }

    if ($titreTrois != "" || $codeTrois != "") {
        if(empty($_POST['titreTrois'])){
            $titreTroisErr = "Le titre du programme est obligatoire!";
        }else if(empty($_POST['codeTrois'])){
            $codeTroisErr = "Le code du programme est obligatoire!";
        }else if(empty($_POST['regimeTrois'])){
            $regimeTroisErr = "Le regime est obligatoire!";
        }else if(empty($_POST['typeTrois'])){
            $typeTroisErr = "Le type du programme est obligatoire!";
        }
    }

    $cpMinistere = $_COOKIE['myCookie'];
    if (empty($sessionErr) && empty($anneeErr) && empty($titreUnErr) && empty($codeUnErr) && empty($regimeUnErr) && empty($typeUnErr)
        && empty($titreDeuxErr) && empty($codeDeuxErr) && empty($regimeDeuxErr) && empty($typeDeuxErr) && empty($titreTroisErr)
        && empty($codeTroisErr) && empty($regimeTroisErr) && empty($typeTroisErr) ){
        header("Location: ../secondaire.html", true, 303);
        $myfile = fopen("../$cpMinistere.txt", "a+") or die("unable to open file!");
        $temp = "\n" . "Session: " . $session . ", Annee: " . $annee . ", Le titre de premier programme: " . $titreUn . ", Le code de 1e Programme: " . $codeUn .
            ", Le regime de 1e programme: " . $regimeUn . ", Le type de 1e programme: " . $typeUn . ", Le titre de deuxieme programme: " . $titreDeux .
            ", Le code de 2e programme: " . $codeDeux . ", Le regime de 2e programme: " . $regimeDeux . ", Le type de 2e programme: " . $regimeDeux .
            ", Le titre de troisieme programme: " . $titreTrois . ", Le code de 3e programme: " . $codeTrois . ", Le regime de 3e programme: " . $regimeTrois .
            ", Le type de 3e programme: " . $typeTrois. "\n";
        $text = utf8_encode($temp);
        $text = "\xEF\xBB\xBF".$temp;
        fwrite($myfile, $text);
        fclose($myfile);
        exit;
    }else{
        header("location: erreur.php", true, 400);
        exit;
    }
    }
?>