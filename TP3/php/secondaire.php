<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $derniereAnnee = $_POST['derniereAnnee'];
        $derAnneeDebut = $_POST['derAnneeDebut'];
        $derAnneeFin = $_POST['derAnneeFin'];
        $diplomeExterieur = $_POST['diplomeExterieur'];
        $nomDiplome = $_POST['nomDiplome'];
        $secondaireDebut = $_POST['secondaireDebut'];
        $secondaireFin = $_POST['secondaireFin'];
        $mois = $_POST['mois'];
        $annee = $_POST['annee'];
        $discipline = $_POST['discipline'];
        $institution = $_POST['institution'];
        $pays = $_POST['pays'];
        $dec = $_POST['dec'];
        $decDebut = $_POST['decDebut'];
        $decFin = $_POST['decFin'];
        $decMois = $_POST['decMois'];
        $decAnnee = $_POST['decAnnee'];
        $decDiscipline = $_POST['decDiscipline'];
        $decInstitution = $_POST['decInstitution'];
        $obtenu = $_POST['obtenu'];



    if ($diplomeExterieur != "") {
        if(empty($_POST['nomDiplome'])){
            $nomDiplomeErr = "Le nom du diplôme est obligatoire!";
        }else if(empty($_POST['secondaireDebut'])){
            $secondaireDebutErr = "Le début d'année est obligatoire!";
        }else if(empty($_POST['secondaireFin'])){
            $secondaireFinErr = "La fin d'année est obligatoire!";
        }else if(empty($_POST['mois'])){
            $moisErr = "La mois est obligatoire!";
        }else if(empty($_POST['annee'])){
            $anneeErr = "L'année est obligatoire!";
        }else if(empty($_POST['discipline'])){
            $disciplineErr = "Discipline ou spécialisation est obligatoire!";
        }else if(empty($_POST['institution'])){
            $institutionErr = "Institution où vous avez poursuivi vos étude est obligatoire!";
        }else if(empty($_POST['pays'])){
            $paysErr = "Le pays est obligatoire!";
        }
    }

    if ($dec != "") {
        if(empty($_POST['decDebut'])){
            $decDebutErr = "Le début d'année est obligatoire!";
        }else if(empty($_POST['decFin'])){
            $decFinErr = "La fin d'année est obligatoire!";
        } else if(empty($_POST['decMois'])){
            $decMoisErr = "La mois est obligatoire!";
        }else if(empty($_POST['decAnnee'])){
            $decAnneeErr = "L'année est obligatoire!";
        }else if(empty($_POST['decDiscipline'])){
            $decDisciplineErr = "Discipline ou spécialisation est obligatoire!";
        }else if(empty($_POST['decInstitution'])){
            $decInstitutionErr = "Institution où vous avez poursuivi vos étude est obligatoire!";
        }else if(empty($_POST['obtenu'])){
            $obtenuErr = "Il faut choisir obtenu, à obtenir ou ne sera pas obtenu!";
        }
    }

    $cpMinistere = $_COOKIE['myCookie'];
    if (empty($nomDiplomeErr) && empty($secondaireDebutErr) && empty($secondaireFinErr) && empty($moisErr) && empty($anneeErr)
        && empty($disciplineErr) && empty($institutionErr) && empty($paysErr) && empty($decDebutErr) && empty($decFinErr)
        && empty($decMoisErr) && empty($decAnneeErr) && empty($decDisciplineErr) && empty($decInstitutionErr) && empty($obtenuErr)
        && empty($dateErr)) {
        header("Location: ../universitaire.html", true, 303);
        $myfile = fopen("../$cpMinistere.txt", "a+") or die("unable to open file!");
        $temp = "\n" . " Derniere annee du secondaire au Quebec: " . $derniereAnnee . ", Debut d'année: " . $derAnneeDebut .
            ", La fin d'annee: " . $derAnneeFin . ", Le diplôme à l’extérieur du Québec: " . $diplomeExterieur . ", Le nom du diplôme: " .
            $nomDiplome . ", Le debut d'année: " . $secondaireDebut . ", La fin d'annee: " . $secondaireFin . ", Discipline: " .
            $discipline . ", Le mois d'obtension" . $mois . ", L'année d'obtension: " . $annee. ", Institution: " .
            $institution . ", Pays: " . $pays . ", Autre diplome: " . $dec . ", Le début d'annee: " . $decDebut . ", La fin d'annee: " .
            $decFin . ", Le mois d'obtension: " . $decMois. ", L'annnee d'obtension: " . $decAnnee . ", Discipline: " . $discipline .
            ", Institution: " . $institution . ", Obtenu: " . $obtenu . "\n";
        $text = utf8_encode($temp);
        $text = "\xEF\xBB\xBF".$temp;
        fwrite($myfile, $text);
        fclose($myfile);
        exit;
    }else{
        header("location: erreur.php", true, 400);
        exit;
    }


    }



?>