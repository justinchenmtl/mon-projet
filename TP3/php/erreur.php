<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Erreur page</title>
            <link rel="stylesheet" href="../css/corp.css"/>
        </head>

        <body class="fr">
            <link rel="stylesheet" href="../css/tete.css"/>
            <link rel="stylesheet" href="../css/pied.css"/>

            <div class="section_top" id="top">
                <a class="web_logo" href="../inscription.html">
                    <img src="../css/img/uqam-logo.jpg" alt="UQAM Logo"/>
                </a>
                <p>Admission d'étude à l'UQAM</p>
            </div>
            <div class="quitter">
                <a href="">Aide |</a>
                <a href="">Quitter</a>

            </div>

            <div class="head" id="head">
                <p style="text-align: right">
                    Demande d'admission aux études de premier cycle
                </p>
                <hr>
                <h2 style="text-align: center; color: red">
                    HTTP ERROR 400! <br>
                </h2>
                <h5 style="text-align: center; color: red">
                    This page isn’t working!<br>
                    If the problem continues, contact the site owner.
                </h5>
                <hr>
            </div>



        </body>
    </html>